package com.main.springboot01.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author hzq
 * @description HelloWorld
 * @date 2020/7/17 14:49
 */
@Slf4j
@RestController
public class HelloController {
    @GetMapping("hello")
    public String hello(){
        log.info("hello");
        return "HelloWorld";
    }
}
